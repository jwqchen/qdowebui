//This file contains all javascript helper functions specific to each html template

'use strict';

/* Controllers allow you make javascript helper functions */
var qdoControllers = angular.module('qdoControllers', []);

//think of $scope as variables/functions for a given page template
//think of $rootScope variables/functions as global variables across all templates
// $stateParams are parts of url that are stored in variables


/* controller for home/log in page */
//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
qdoControllers.controller('homeCtrl', function ($scope, $rootScope, $location, localStorageService, Auth, HostFactory) { //localstorage is an angularjs library, Auth is a set of service functions

    $scope.logIn = function (){
        //grab the host of the api first
        var apiHost = HostFactory.query();
        apiHost.$promise.then(function(data){
                $rootScope.apiHost = data.apiHost;

                //think of $scope as variables for a given page template, username and password were stored in the scope by the nd-model directive
                var token = Auth.setCredentials($scope.username, $scope.password).success(function(data, status, headers, config) {
                    //if Auth.setCredentials is successful, do the following
                    token = data.token;
                    var expiration = data.expires;
                    localStorageService.set("token", {  /*store the token locally*/
                      "username": $scope.username, 
                      "apiHost": $rootScope.apiHost,
                      "token": token, 
                      "expiration" : expiration
                    });
                    $rootScope.token = token;
                    $rootScope.credentialsAuthorized = true;
                    $location.path( '/home/' + $scope.username );
                }).error(function(data, status, headers, config) { //if Auth.setCredentials fails, do the following

                    if(status == 304 || status == 404) {
                        $scope.error = "Whoops, doesn't look like that your webserver is running." ;
                    }
                    else{
                        $scope.error = "Whoops! Please try logging in again." ;
                    }

                    $rootScope.credentialsAuthorized = false;

                });
        });

        
    };

});





/* controller for userhome */
qdoControllers.controller('userhomeCtrl', //'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
    function ($scope, $rootScope, $stateParams, $location, QueueFactory, localStorageService, queues, $interval) {
        //QueueFactory is a set of service functions
        // localStorageService is an angularjs library
        // queues variable was declared in qdoApp.js
        //think of $scope as variables/functions for a given page template
        //think of $rootScope variables/functions as global variables across all templates
        // $stateParams are parts of url that are stored in variables
       
        $scope.username = $stateParams.username;

        $rootScope.queues = queues;
        $scope.sortField = 'name';
        $scope.sortReverse  = false;
        $scope.itemsPerPage = 10;
        $scope.queuesChecked = false;
        $scope.checkAllQueues = false;

        var pointer = 0;
        for ( var i=0; i < queues.length; i++) { 
          pointer += 1;
        };

        if(!$rootScope.checkList) {
            for(var i = 0; i < $rootScope.queues.length; i++) {
                $rootScope.queues[i].checked = false;
            }

            $rootScope.checkList = true;
        }

        $scope.nqueues = pointer; /*number of queues*/

        /*$scope.ntasks = function(queue) {
            var totaltasks = 0;
            for ( var state in queue["ntasks"]) {      
              totaltasks += queue["ntasks"][state];
            };
            return totaltasks;
        };*/

        $(document).tooltip({
          position: {
            my: "center bottom-20",
            at: "center top",
            using: function( position, feedback ) {
              $( this ).css( position );
              $( "<div>" )
                .addClass( "arrow" )
                .addClass( feedback.vertical )
                .addClass( feedback.horizontal )
                .appendTo( this );
            }
          }
        });

        setTimeout(function() {
            componentHandler.upgradeElements($('.js-queues-table .mdl-tooltip').get());
        }, 1000);

        $scope.ntasks = function(queue, stateName) {
            var totaltasks = 0;

            if(queue && queue !== "*") {
                if(stateName) {
                    return queue["ntasks"][stateName];
                }

                for ( var state in queue["ntasks"]) {      
                    totaltasks += queue["ntasks"][state];
                };      
            }

            else {
                for(var i = 0; i < $rootScope.queues.length; i++) {
                    if(stateName) {
                        totaltasks += $rootScope.queues[i]["ntasks"][stateName];
                    }
                    else {
                        for ( var state in $rootScope.queues[i]["ntasks"]) {      
                          totaltasks += $rootScope.queues[i]["ntasks"][state];
                        };
                    }   
                }
            }
            return totaltasks;
        };        
        
        $scope.percent = function(queue, status) {
            var totaltasks = $scope.ntasks(queue);
            if (totaltasks > 0){
              return 100.0 * queue["ntasks"][status] / totaltasks;
            }else{
              return 0;
            };
        };
        $scope.succeeded = function(queue) {return $scope.percent(queue, "Succeeded") == 100;};
        $scope.failed = function(queue) {return $scope.percent(queue, "Failed") > 0;};
        $scope.inProgress = function(queue) {
            if($scope.ntasks(queue)>0){
                return !$scope.succeeded(queue) && !$scope.failed(queue) && $scope.ntasks(queue) > 0;
            }else{
                return true;
            };
        };


        $scope.isActive = function(queue){return queue["state"]=="Active";};
        $scope.isPaused = function(queue){return queue["state"]=="Paused";};

        $scope.pause = function(queueName){
            
            QueueFactory.pause($scope.username, queueName).success(function(data, status, headers, config) {
  
                //if createQueue is successful do this
                QueueFactory.getQueues($scope.username).success(function(data, status, headers, config) {
                    //if getQueues is successful do this
                        $rootScope.queues = data.queues;//remember to update the queues variable so the page live updates
                });
            });

        };

        //NOTE: resume, retry, rerun, addTask all call Service funcions and behave similarly to $scope.pause function, please refer to those notes
        $scope.resume = function(queueName){
            QueueFactory.resume($scope.username, queueName).success(function(data, status, headers, config) {
                //if createQueue is successful do this
                QueueFactory.getQueues($scope.username).success(function(data, status, headers, config) {
                    //if getQueues is successful do this
                        $rootScope.queues = data.queues;//remember to update the queues variable so the page live updates
                });
            });
        };  

        $scope.logOut = function(){
          $location.path( '/home' );
          $rootScope.$on("$locationChangeSuccess", function(event) { 
            localStorageService.remove("token");
          });
        };

        $scope.storeQueueName = function (queuename) {
            $rootScope.queuename = queuename;
        };

        $scope.createQueue = function () {
            // Queuefactory is a set of service.js functions

            if($scope.newQueueName) {

                QueueFactory.createQueue($scope.username, $scope.newQueueName).success(function(data, status, headers, config) {
                    //if createQueue is successful do this

                    alert('Queue has been created!');

                    QueueFactory.getQueues($scope.username).success(function(data, status, headers, config) {
                        //if getQueues is successful do this
                            $rootScope.queues = data.queues;//remember to update the queues variable so the page live updates
                    });
                });

            }
        };

        $scope.createAndLaunch = function() {

            var tasks = $scope.taskList.trim().split('\n');

            var priorities = $scope.priorityList.trim().split('\n');

            console.log(tasks);

            if((tasks.length == priorities.length) || priorities.length <= 1) {
                
            }

            //QueueFactory.createQueue($scope.username, $scope.newQueueName).success(function(data, status, headers, config) {
                /*QueueFactory.addTask($scope.username, $scope.queuename, $scope.newTask, $scope.priority).success(function(data, status, headers, config) {

                });*/
            //});
        };

        $scope.rerun = function(queuename){
            QueueFactory.rerun($scope.username, queuename).success(function(data, status, headers, config) {
            
            });
        };

        $scope.retry = function(queuename){
            QueueFactory.retry($scope.username, queuename).success(function(data, status, headers, config) {
                
            });
        };

        $scope.recover = function(queuename){
            QueueFactory.recover($scope.username, queuename).success(function(data, status, headers, config) {
                
            });
        };

        $scope.checkQueue = function(queue, index) {
            $rootScope.queues[index].checked = !$rootScope.queues[index].checked;

            if($('.js-check-table:checked').length == 0) {
                $scope.queuesChecked = false;
            }
            else {
                $scope.queuesChecked = true;
            }
        };

        $scope.deleteQueue = function($event) {
            var queueToDelete = $($event.currentTarget).attr('data-queue');
            var delQueue = confirm('Are you sure you want to delete this queue?');

            if(delQueue) {
                QueueFactory.deleteQueue($scope.username, queueToDelete)
                .success(function(data, status, headers, config) {
                    alert('Queue deleted');
                    QueueFactory.getQueues($scope.username).success(function(data, status, headers, config) {
                        //if getQueues is successful do this
                            $rootScope.queues = data.queues;//remember to update the queues variable so the page live updates
                    });
                })
                .error(function(data, status, headers, config) {
                    console.log(('delete failed'), status);
                });
            }
        };

        $scope.toggleCreateQueuePage = function() {
            $('.js-queue-list-page').toggle();
            $('.js-create-queue-page').toggle();
        }

        // $scope.toggleMultiSelectBar = function() {
        //     $('.js-multi-select-bar button').each(function() {
        //         if($(this).is(':disabled')) {
        //             $(this).removeAttr('disabled');
        //         }
        //         else {
        //             $(this).attr('disabled', 'disabled');
        //         }
        //     });
        // };

        // $scope.toggleMultiSelectBar = function() {
        //     var anyChecked = false;
        //     for (var i = 0; i < $rootScope.queues.length; i++){
        //         if($rootScope.queues[i].checked){
        //             anyChecked = true;
        //             $('.js-multi-select-bar button').each(function(){
        //                 $(this).removeAttr('disabled');
        //             });
        //             break;
        //             }
        //         else{
        //             $('.js-multi-select-bar button').each(function(){
        //                 $(this).attr('disabled', 'disabled');
        //             });
        //             }
        //     }
        // };

        $scope.toggleMultiSelectBar = function() {
            if ($scope.queuesChecked == false){
                $('.js-multi-select-bar button').each(function(){
                    $(this).attr('disabled', 'disabled');
                });              
            }
            else {
                $('.js-multi-select-bar button').each(function(){
                    $(this).removeAttr('disabled');
                });
            }
        };

        // $scope.pause = function(queueName){
        //     QueueFactory.pause($scope.username, queueName).success(function(data, status, headers, config) {
  
        //         //if createQueue is successful do this
        //         QueueFactory.getQueues($scope.username).success(function(data, status, headers, config) {
        //             //if getQueues is successful do this
        //                 $rootScope.queues = data.queues;//remember to update the queues variable so the page live updates
        //         });
        //     });
        // };

        // //NOTE: resume, retry, rerun, addTask all call Service funcions and behave similarly to $scope.pause function, please refer to those notes
        // $scope.resume = function(queueName){
        //     QueueFactory.resume($scope.username, queueName).success(function(data, status, headers, config) {
        //         //if createQueue is successful do this
        //         QueueFactory.getQueues($scope.username).success(function(data, status, headers, config) {
        //             //if getQueues is successful do this
        //                 $rootScope.queues = data.queues;//remember to update the queues variable so the page live updates
        //         });
        //     });
        // };  

        $scope.toggleAllQueues = function() {
            $('.js-check-table').each(function() {
                if($scope.checkAllQueues) {
                    $(this).prop('checked', true);
                    $scope.queuesChecked = true;
                }
                else {
                    $(this).prop('checked', false);
                    $scope.queuesChecked = false;
                }
            });
        };

        $scope.queueChecked = function() {
            alert('checked');
        };


        $scope.multiQueueResume = function() {
            for (var i = 0; i < $rootScope.queues.length; i++){
                //the second if condition appears always false...
                if($rootScope.queues[i].checked && $scope.isPaused($rootScope.queues[i])){
                    QueueFactory.resume($scope.username, $rootScope.queues[i].name).success(function(data, status, headers, config){

                    })
                }
            }
        };


        
        $scope.multiQueuePause = function() {
            for (var i = 0; i < $rootScope.queues.length; i++){
                if($rootScope.queues[i].checked && $scope.isActive($rootScope.queues[i])){
                    QueueFactory.pause($scope.username, $rootScope.queues[i].name).success(function(data, status, headers, config){

                    });
                };
            };
        };

        $scope.multiQueueRerun = function() {
            for(var i = 0; i < $rootScope.queues.length; i++) {
                if($rootScope.queues[i].checked){
                    QueueFactory.rerun($scope.username, $rootScope.queues[i].name).success(function(data, status, headers, config) {
                
                    });
                }
            }
        };

        $scope.multiQueueRetry = function() {
            for(var i = 0; i < $rootScope.queues.length; i++) {
                if($rootScope.queues[i].checked){
                    QueueFactory.retry($scope.username, $rootScope.queues[i].name).success(function(data, status, headers, config) {
                
                    });
                }
            }
        };

    $interval(function() {
        var homeURL = '/home/' + $stateParams.username;
        var currentURL = $location.url();
        if($rootScope.credentialsAuthorized && currentURL === homeURL) {
            QueueFactory.getQueues($stateParams.username, true).success(function(data, status, headers, config) {//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
            console.log('auto refreshing userhome page');
                      //if the getQueues function is successful.. do the following

                for(var i = 0; i < $rootScope.queues.length; i++) {
                    data.queues[i].checked = $rootScope.queues[i].checked;
                }

                $rootScope.queues = data.queues;

            }).error(function(data, status, headers, config) {
                console.log('auto refresh error');
                $rootScope.credentialsAuthorized = false;
                $location.path( '/home' );
            });
        }
    }, $rootScope.refreshInterval);


    });


/* controller for Delete Modal Window */
//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
qdoControllers.controller('deleteQueueController', function($scope, $rootScope, $stateParams, QueueFactory, ngDialog) {
    //QueueFactory is a set of service functions
    //think of $scope as variables/functions for a given page template
    //think of $rootScope variables/functions as global variables across all templates
    // $stateParams are parts of url that are stored in variables
    // ngDialog ia an angularjs library pertaining to popup dialog windows

    $scope.username = $stateParams.username;
    $scope.loading1 = false;

    $scope.deleteQueue = function(){
        /*for(var i = 0; i < $rootScope.queues.length; i++) {
            if($rootScope.queues[i].checked){
                QueueFactory.deleteQueue($scope.username, $rootScope.queues[i].name)
                .success(function(data, status, headers, config) {
                    console.log(('success'), status);
                    $scope.loading1 = true;
                    ngDialog.close();//close dialog window
                })
                .error(function(data, status, headers, config) {
                    console.log(('delete failed'), status);
                });
            };
        };*/

        QueueFactory.deleteQueue($scope.username, '')
        .success(function(data, status, headers, config) {

        })
        .error(function(data, status, headers, config) {
            console.log(('delete failed'), status);
        });
    };



    $scope.closeDeleteModal = function () {
        ngDialog.close();
    };



});


/* controller for queuepage */
qdoControllers.controller('queueCtrl', //'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
    function ($scope, $rootScope, $stateParams, $location, QueueFactory, localStorageService, queue, queues, queueTaskDetails, $interval) {
        //QueueFactory is a set of service functions
        // localStorageService is an angularjs library
        // queue variable was declared in qdoApp.js
        // queueTaskDetails variable was declared in qdoApp.js
        //think of $scope as variables/functions for a given page template
        //think of $rootScope variables/functions as global variables across all templates
        // $stateParams are parts of url that are stored in variables
       
        $scope.username = $stateParams.username;
        $scope.queuename = $stateParams.queuename;
        $rootScope.queuename = $stateParams.queuename;
        $scope.queue = queue;
        $scope.queues = queues;
        $scope.queueTaskDetails = queueTaskDetails;

        $scope.showTaskDetailsTable = true;
        $scope.sortField = 'task';
        $scope.sortReverse  = false;

        //Launch queue parameters
        $scope.hostName = '';
        $scope.batchQueue = '';
        $scope.parallelization = '';

        $(document).tooltip({
          position: {
            my: "center bottom-20",
            at: "center top",
            using: function( position, feedback ) {
              $( this ).css( position );
              $( "<div>" )
                .addClass( "arrow" )
                .addClass( feedback.vertical )
                .addClass( feedback.horizontal )
                .appendTo( this );
            }
          }
        });

        $scope.showHideTaskDetailsTable = function(){
            if($scope.showTaskDetailsTable == false){
                $scope.showTaskDetailsTable = true;
            }else{
                $scope.showTaskDetailsTable = false;
            };
        };

        $scope.showLaunchDetails = false;
        $scope.showHideLaunchDetails = function(){
            if($scope.showLaunchDetails == false){
                $scope.showLaunchDetails = true;
            }else{
                $scope.showLaunchDetails = false;
            };
        };


        $scope.ntasks = function(queue, stateName) {
            var totaltasks = 0;

            if(queue && queue !== "*") {
                if(stateName) {
                    return queue["ntasks"][stateName];
                }

                for ( var state in queue["ntasks"]) {      
                    totaltasks += queue["ntasks"][state];
                };      
            }

            else {
                for(var i = 0; i < $rootScope.queues.length; i++) {
                    if(stateName) {
                        totaltasks += $rootScope.queues[i]["ntasks"][stateName];
                    }
                    else {
                        for ( var state in $rootScope.queues[i]["ntasks"]) {      
                          totaltasks += $rootScope.queues[i]["ntasks"][state];
                        };
                    }   
                }
            }
            return totaltasks;
        };
        
        $scope.percent = function(queue, status) {
            var totaltasks = $scope.ntasks(queue);
            if (totaltasks > 0){
              return 100.0 * queue.qdo_result["ntasks"][status] / totaltasks;
            }else{
              return 0;
            };
        };
        $scope.succeeded = function(queue) {return $scope.percent(queue, "Succeeded") == 100;};
        $scope.failed = function(queue) {return $scope.percent(queue, "Failed") > 0;};
        $scope.hasRunningTask = function(queue) {return $scope.percent(queue, "Running") > 0;};
        $scope.inProgress = function(queue) {
            if($scope.ntasks(queue)>0){
                return !$scope.succeeded(queue) && !$scope.failed(queue) && $scope.ntasks(queue) > 0;
            }else{
                return true;
            };
        };

        $scope.isActive = function(queue){return queue.qdo_result["state"]=="Active";};
        $scope.isPaused = function(queue){return queue.qdo_result["state"]=="Paused";};

        $scope.logOut = function(){
          $location.path( '/home' );
          $rootScope.$on("$locationChangeSuccess", function(event) { 
            localStorageService.remove("token");
          });
        };

        $scope.goToUserhomePage = function(){$location.path( '/home/' + $scope.username );};

        $scope.pause = function(){
            QueueFactory.pause($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                //if pause is successful do the following
                QueueFactory.getQueue($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                    //if getQueue is successful then remember to do a update of variables so the page loads lives
                    $scope.queue = data;
                });
                QueueFactory.getQueueTaskDetails($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                    //if getQueueTaskDEtails is successful then remember to do a update of variables so the page loads lives
                    $scope.queueTaskDetails = data.tasks;
                });
            });
        };

        //NOTE: resume, retry, rerun, addTask all call Service funcions and behave similarly to $scope.pause function, please refer to those notes
        $scope.resume = function(){
            QueueFactory.resume($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                QueueFactory.getQueue($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                    $scope.queue = data;
                });
                QueueFactory.getQueueTaskDetails($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                    $scope.queueTaskDetails = data.tasks;
                });
            });
        };
        $scope.retry = function(){
            QueueFactory.retry($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                QueueFactory.getQueue($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                    $scope.queue = data;
                });
                QueueFactory.getQueueTaskDetails($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                    $scope.queueTaskDetails = data.tasks;
                });
            });
        };
        $scope.rerun = function(){
            QueueFactory.rerun($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                QueueFactory.getQueue($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                    $scope.queue = data;
                });
                QueueFactory.getQueueTaskDetails($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                    $scope.queueTaskDetails = data.tasks;
                });
            });
        };
        $scope.recover = function(){
            QueueFactory.recover($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                QueueFactory.getQueue($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                    $scope.queue = data;
                });
                QueueFactory.getQueueTaskDetails($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                    $scope.queueTaskDetails = data.tasks;
                });
            });
        };

        $scope.addTask = function () {
            if(!$scope.priority){
                $scope.priority = 0; //$scope.priority was declared by an ng-model call in queue.html
            };
            QueueFactory.addTask($scope.username, $scope.queuename, $scope.newTask, $scope.priority).success(function(data, status, headers, config) {

                alert('Task has been added');

                QueueFactory.getQueue($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                    $scope.queue = data;
                });
                QueueFactory.getQueueTaskDetails($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                    $scope.queueTaskDetails = data.tasks;
                });
            });
        };

        $scope.pack = true;
        $scope.mpack = 1;
        $scope.nworkers = 24;

        $scope.launchQueue = function () {
            QueueFactory.launchQueue($scope.username, $scope.queuename, $scope.pack, $scope.mpack, $scope.nworkers, $scope.site, $scope.script, $scope.timeout, $scope.runtime, $scope.batchqueue, $scope.walltime, $scope.batch_opts, $scope.verbose)
                    .success(function(data, status, headers, config) {
                        alert("Queue "+ $scope.queuename + " has been launched!");
                        $scope.showLaunchDetails = false;
                        console.log("launch success!");
                });

        };

        $scope.deleteQueue = function($event) {
            var delQueue = confirm('Are you sure you want to delete this queue?');

            if(delQueue) {
                QueueFactory.deleteQueue($scope.username, $rootScope.queuename)
                .success(function(data, status, headers, config) {
                    alert('Queue deleted');
                    $location.path( '/home/' + $scope.username );
                    /*QueueFactory.getQueues($scope.username).success(function(data, status, headers, config) {
                        //if getQueues is successful do this
                            $rootScope.queues = data.queues;//remember to update the queues variable so the page live updates
                    });*/
                })
                .error(function(data, status, headers, config) {
                    console.log(('delete failed'), status);
                });
            }
        };

        $scope.toggleCreateTaskPage = function() {
            $('.js-queue-page').toggle();
            $('.js-add-task-dialog').toggle();
        }

        $scope.toggleLaunchQueuePage = function() {
            $('.js-queue-page').toggle();
            $('.js-launch-queue-dialog').toggle();
        }

        $scope.addTasks = function () {

            var tasks = $scope.taskList.trim().split('\n');

            var priorities = $scope.priorityList.trim().split('\n');

            console.log(tasks);

            if((tasks.length == priorities.length) || priorities.length <= 1) {
                for(var i=0; i< tasks.length; i++) {
                    QueueFactory.addTask($scope.username, $scope.queuename, tasks[i], $scope.priority).success(function(data, status, headers, config) {
                        /*QueueFactory.getQueue($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                            $scope.queue = data;
                        });
                        QueueFactory.getQueueTaskDetails($scope.username, $scope.queuename).success(function(data, status, headers, config) {
                            $scope.queueTaskDetails = data.tasks;
                        });*/
                    });
                }
            }
        };

      $interval(function() {
        var queueURL = '/home/' + $stateParams.username + '/queues/' + $rootScope.queuename;

        var currentURL = $location.url();
          if($rootScope.credentialsAuthorized && currentURL === queueURL) {
                    console.log('auto refreshing queue page');
                  QueueFactory.getQueue($scope.username, $scope.queuename, true).success(function(data, status, headers, config) {//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
                      $scope.queue = data;

                  }).error(function(data, status, headers, config) {
                      console.log('auto refresh error');
                      $rootScope.credentialsAuthorized = false;
                      $location.path( '/home' );
                  });
          };
      }, $rootScope.refreshInterval);

});

qdoControllers.controller('failedTasksCtrl', 
    function ($scope, $rootScope, $stateParams, $location, QueueFactory, localStorageService, queues, $interval) {
        //QueueFactory is a set of service functions
        // localStorageService is an angularjs library
        // queues variable was declared in qdoApp.js
        //think of $scope as variables/functions for a given page template
        //think of $rootScope variables/functions as global variables across all templates
        // $stateParams are parts of url that are stored in variables
       
        $scope.username = $stateParams.username;
        $scope.pageState = $stateParams.pageState;

        $rootScope.queues = queues;

        var pointer = 0;
        for ( var i=0; i < queues.length; i++) { 
          pointer += 1;
        };

        $scope.nqueues = pointer; /*number of queues*/

        $rootScope.tasks = [];

        for(var i=0;i<$rootScope.queues.length;i++) {
            QueueFactory.getQueueTaskDetails($stateParams.username, $rootScope.queues[i].name).success(function(data, status, headers, config) {
            //if the getQueues function is successful.. do the following
                $rootScope.tasks.push(data.tasks)
            // $rootScope.loading = false;
            }).error(function(data, status, headers, config) {

            });
        }

        $scope.ntasks = function(queue, stateName) {
            var totaltasks = 0;

            if(queue && queue !== "*") {
                if(stateName) {
                    return queue["ntasks"][stateName];
                }

                for ( var state in queue["ntasks"]) {      
                    totaltasks += queue["ntasks"][state];
                };      
            }

            else {
                for(var i = 0; i < $rootScope.queues.length; i++) {
                    if(stateName) {
                        totaltasks += $rootScope.queues[i]["ntasks"][stateName];
                    }
                    else {
                        for ( var state in $rootScope.queues[i]["ntasks"]) {      
                          totaltasks += $rootScope.queues[i]["ntasks"][state];
                        };
                    }   
                }
            }
            return totaltasks;
        };
    }
);

qdoControllers.controller('succeededTasksCtrl', 
    function ($scope, $rootScope, $stateParams, $location, QueueFactory, localStorageService, queues, $interval) {
        //QueueFactory is a set of service functions
        // localStorageService is an angularjs library
        // queues variable was declared in qdoApp.js
        //think of $scope as variables/functions for a given page template
        //think of $rootScope variables/functions as global variables across all templates
        // $stateParams are parts of url that are stored in variables
       
        $scope.username = $stateParams.username;
        $scope.pageState = $stateParams.pageState;

        $rootScope.queues = queues;

        var pointer = 0;
        for ( var i=0; i < queues.length; i++) { 
          pointer += 1;
        };

        $scope.nqueues = pointer; /*number of queues*/

        $rootScope.tasks = [];

        for(var i=0;i<$rootScope.queues.length;i++) {
            QueueFactory.getQueueTaskDetails($stateParams.username, $rootScope.queues[i].name).success(function(data, status, headers, config) {
            //if the getQueues function is successful.. do the following
                $rootScope.tasks.push(data.tasks)
            // $rootScope.loading = false;
            }).error(function(data, status, headers, config) {

            });
        }

        $scope.ntasks = function(queue, stateName) {
            var totaltasks = 0;

            if(queue && queue !== "*") {
                if(stateName) {
                    return queue["ntasks"][stateName];
                }

                for ( var state in queue["ntasks"]) {      
                    totaltasks += queue["ntasks"][state];
                };      
            }

            else {
                for(var i = 0; i < $rootScope.queues.length; i++) {
                    if(stateName) {
                        totaltasks += $rootScope.queues[i]["ntasks"][stateName];
                    }
                    else {
                        for ( var state in $rootScope.queues[i]["ntasks"]) {      
                          totaltasks += $rootScope.queues[i]["ntasks"][state];
                        };
                    }   
                }
            }
            return totaltasks;
        };
    }
);