//Think of this file as full of helper javaScript functions for the controller functions. 
//This is where you make javaScript functions that call other pages. GETs, PUTs, POSTs, DELETEs...etc.
//these functions are not specific to any particular html template and are called in controllers.js
//NOTE: $http is a special angularjs object

'use strict'

var qdoServices = angular.module('qdoServices', ['ngResource']);

//retrieves the host.json file which you can manually edit to change the host of the api
qdoServices.factory('HostFactory', function ($resource) {
        return $resource('./config/apiHost.json', {}, {
        query: { method: 'GET' },
    })

});



//Auth deals with logging in
//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
qdoServices.factory('Auth', ['$base64', '$rootScope', '$http', function ($base64, $rootScope, $http) {
    return {
        setCredentials: function (username, password) {
            return $http({
              method: 'POST', 
              url:  $rootScope.apiHost + '/api/v1/site/login/', 
              data: 'username=' + username + "&password=" + password,
              withCredentials: true,
              headers: {
                //'Access-Control-Allow-Credentials': '',
                'Content-Type': 'application/x-www-form-urlencoded'
              }
            });
        },
    };
}]);


//QueueFactory functions retrieve the qdo queues and manipulate them
//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
qdoServices.factory('QueueFactory', ['$base64', '$rootScope', '$http', 'localStorageService', function ($base64, $rootScope, $http, localStorageService) {


  return {
      getQueues: function (username, ignoreLoadingBar) {
          //console.log($rootScope.token);
          if(!$rootScope.apiHost && $rootScope.credentialsAuthorized) {
              var token = localStorageService.get('token');
              $rootScope.apiHost = token.apiHost;
          }

          return $http({
              method: 'GET', 
              //withCredentials: true,

              url: $rootScope.apiHost + '/api/v1/' + username + '/queues/', 
              headers: {
                //'Access-Control-Allow-Credentials': '' 
              },
              ignoreLoadingBar: ignoreLoadingBar,
            });
      },


      getQueue: function (username, queuename, ignoreLoadingBar) {
          //console.log($rootScope.token);
          if(!$rootScope.apiHost && $rootScope.credentialsAuthorized) {
              var token = localStorageService.get('token');
              $rootScope.apiHost = token.apiHost;
          }

          return $http({
              method: 'GET', 
              url: $rootScope.apiHost + '/api/v1/' + username + '/queues/' + queuename + '/', 
              headers: {},
              ignoreLoadingBar: ignoreLoadingBar,	
            });
      },
      testRemote: function (username) {
          //console.log($rootScope.token);
          return $http({
              method: 'GET', 
              url: $rootScope.apiHost + '/api/v1/site/testremote/', 
              headers: {}
            });
      },
      setupRemote: function (username) {
          //console.log($rootScope.token);
          return $http({
              method: 'POST', 
              url: $rootScope.apiHost + '/api/v1/site/setup/', 
              headers: {}
            });
      },
      testLogin: function (username) {
          //console.log($rootScope.token);
          return $http({
              method: 'GET', 
              url: $rootScope.apiHost + '/api/v1/site/login/', 
              headers: {}
            });
      },
      getQueueTaskDetails: function(username, queuename){
            //console.log($rootScope.token);
          //console.log('http://0.0.0.0:8080/api/v1/' + username + '/queues/' + queuename + "/tasks/");
            return $http({
              method: 'GET', 
              url: $rootScope.apiHost + '/api/v1/' + username + '/queues/' + queuename + "/tasks/", 
              headers: {},
            });
      },
      pause: function(username, queuename){
          return $http({
              method: 'PUT', 
              url: $rootScope.apiHost + '/api/v1/' + username + '/queues/' + queuename + '/', 
              data : 'state=Paused',
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},     
          });       
      },
      resume: function(username, queuename){
           return $http({
              method: 'PUT', 
              url: $rootScope.apiHost + '/api/v1/' + username + '/queues/' + queuename + '/', 
              data : 'state=Active',
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            });
      },
      retry: function(username, queuename){
           return $http({
              method: 'PUT', 
              url: $rootScope.apiHost + '/api/v1/' + username + '/queues/' + queuename + '/retry/', 
              headers: {},
            });
      },
      rerun: function(username, queuename){
           return $http({
              method: 'PUT', 
              url: $rootScope.apiHost + '/api/v1/' + username + '/queues/' + queuename + '/rerun/', 
              headers: {},
            });
      },
      recover: function(username, queuename){
           return $http({
              method: 'PUT', 
              url: $rootScope.apiHost + '/api/v1/' + username + '/queues/' + queuename + '/recover/', 
              headers: {},
            });
      },
      deleteQueue: function(username, queuename){
           return $http({
              method: 'DELETE',
              url: $rootScope.apiHost + '/api/v1/' + username + '/queues/' + queuename + '/', 
              headers: {},
            });
      },
      createQueue: function(username, queuename){
           return $http({
              method: 'POST', 
              url: $rootScope.apiHost + '/api/v1/' + username + '/queues/' + queuename + '/', 
              headers: {
                // 'Authorization': 'Basic '+ $base64.encode($rootScope.token + ':' + "not_a_valid_password"), 
                // 'Content-Type': 'application/x-www-form-urlencoded'
              },
            });
      },
      addTask: function(username, queuename, task, priority){
           return $http({
              method: 'POST', 
              url: $rootScope.apiHost + '/api/v1/' + username + '/queues/' + queuename + "/tasks/", 
              data : 'task=' + task + "&priority=" + priority,
              headers: {
                // 'Authorization': 'Basic '+ $base64.encode($rootScope.token + ':' + "not_a_valid_password"), 
                'Content-Type': 'application/x-www-form-urlencoded'
              },
            });
      },
      //some parameters are optional for the API call
      launchQueue: function launchQueue(username, queuename, pack, mpack, nworkers, site, script, timeout, runtime, batchqueue, walltime, batch_opts, verbose){
          // get the names of the parameters.
          function getParamNames(func) {
            var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
            var ARGUMENT_NAMES = /([^\s,]+)/g;
            var fnStr = func.toString().replace(STRIP_COMMENTS, '');
            var result = fnStr.slice(fnStr.indexOf('(')+1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
            if(result === null)
              result = [];
            return result;
          };

          // convert argument values into an array
          function ArgumentsToArray(args) {
            return [].slice.apply(args);
          };

          var paramNames = getParamNames(launchQueue);
   
          //"arguments" gets the values of the arguments passed into the launchQueue function
          var argValue = ArgumentsToArray(arguments);
          var flag = true;
          for (var i = 0; i < argValue.length; i++) {
            if (paramNames[i] !== "username" && paramNames [i] !== "queuename" && argValue[i] !== undefined && argValue[i] !== "" && flag === true){
              var launchData = paramNames[i] + "=" + argValue[i];
              flag = false;
            } else if (paramNames[i] !== "username" && paramNames [i] !== "queuename" && argValue[i] !== undefined && argValue[i] !== "" && flag === false){
              launchData = launchData.concat("&"+ paramNames[i] + "=" + argValue[i]);
            };
          };

          console.log (launchData);


           return $http({
              method: 'PUT', 
              url: $rootScope.apiHost + '/api/v1/' + username + '/queues/' + queuename + "/launch/", 
              data : launchData,
              headers: { 
                'Content-Type': 'application/x-www-form-urlencoded'
              },
            });
      },

      assignVars: function($rootScope, localStorageService) {
          if(!$rootScope.apiHost && $rootScope.credentialsAuthorized) {
              var token = localStorageService.get('token');
              $rootScope.apiHost = token.apiHost;
          }
      }
  }
}]);



//intercepts 401 responses and redirects to the login page
qdoServices.factory('httpInterceptor', function httpInterceptor ($q, $window, $location) {
  return function (promise) {
      var success = function (response) {
          return response;
      };

      var error = function (response) {
          if ((response.status === 401)|| (response.status === 403)) {
              $location.url('/login');
          }

          return $q.reject(response);
      };

      return promise.then(success, error);
  };
});

//adds the auth token to all API calls
qdoServices.factory('api', function ($http, $cookies) {
  return {
      init: function (token) {
          $http.defaults.headers.common['X-Access-Token'] = token || $cookies.token;
      }
  };
});

