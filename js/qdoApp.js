'use strict';

/* qdoApp Module */
//This is the main/parent javascript file, Think of this file as the conductor for all the other javascript files\
//tell your app about all the different templates, their urls and their controllers are in this file

var qdoApp = angular.module('qdoApp', [ //tell quoApp about any javascript modules (directives, controllers, services) here:
  'qdoControllers',
  'qdoServices',
  'qdoDirectives',
  'ui.router',
  'ui.bootstrap',
  'ui.select2',
  'ngCookies',
  'base64',
  'ngDialog',
  'LocalStorageModule',
  'ngSanitize',
  // lib for loading-bar
  'angular-loading-bar',
  'angularUtils.directives.dirPagination',
  
]).run(//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
      [ '$rootScope', '$state', '$stateParams', 'localStorageService', 'QueueFactory', '$location',
      function ($rootScope,   $state,   $stateParams, localStorageService, QueueFactory, $location) { //localstorage is an angularjs library 
        //think of $rootScope variables as global variables across all templates
        // $stateParams are parts of url that are stored in variables 

        //upon first load do all of the things below
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        /*check to see that you already have a token for the day*/
        var currentDate = new Date();
        currentDate = currentDate.getFullYear() + "-" + currentDate.getMonth() + "-" + currentDate.getDay()  + " " 
                      + currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds() 
                      + "." + currentDate.getMilliseconds();
        currentDate = Date.parse(currentDate);
        //localstorage is an angularjs library                   
        if(localStorageService.get("token") && Date.parse(localStorageService.get("token")["expiration"]) > currentDate){ 
            $rootScope.token = localStorageService.get("token")["token"];
            //console.log("grabbing token from local storage");
            //console.log($rootScope.token);
        }

        $rootScope.credentialsAuthorized = true;
        // $rootScope.loading = false;
        // state for turning on remote setup loading message
        $rootScope.dataLoading = false;
        $rootScope.refreshInterval = 1500000;

        $rootScope.$on('$locationChangeStart', function($event, changeTo, changeFrom) {
            console.log(changeFrom);
            console.log(changeTo);

            /*QueueFactory.testLogin($stateParams.username).success(function(data, status, headers, config) {//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
                    //if the getQueues function is successful.. do the following
                    console.log('login confirmed');
                    console.log($location.path());

                }).error(function(data, status, headers, config) {//if the getQueues function fails..do the following
                    console.log("error: could not confirm login");
                    $rootScope.credentialsAuthorized = false;
                    $location.path( '/home' );
                  
            });*/
            
        });


      }]);




// client side routing
qdoApp.config(function($stateProvider, $urlRouterProvider, $httpProvider){//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.

  $httpProvider.responseInterceptors.push('httpInterceptor');

  //setting angular to use credentials so that cookies can be used for auth and retrieving queues - megha 6/8
  $httpProvider.defaults.withCredentials = true;
  
  // For any unmatched url, send to /home
  $urlRouterProvider.otherwise("/home")


  //tell angularjs about all the different templates you have here, what their url is and what controller they talk to.
  $stateProvider
    .state('home', {
        url: "/home",
        templateUrl: "partials/login.html",
        controller: 'homeCtrl',
    })
    .state('userhome', {
        url: "/home/:username",//username is a $stateParam
        templateUrl: "partials/queues.html",
        controller: 'userhomeCtrl',

        resolve: { //resolve tells angularjs to stall loading the template until 'this' stuff has finished
          //'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
          queues: ['$q', 'QueueFactory', '$stateParams', '$rootScope', '$location', 'localStorageService', '$cookies', function($q, QueueFactory, $stateParams, $rootScope, $location, localStorageService, $cookies){
              // $rootScope.loading = true;
              var d = $q.defer();

              $(document).tooltip({
                position: {
                  my: "center bottom-20",
                  at: "center top",
                  using: function( position, feedback ) {
                    $( this ).css( position );
                    $( "<div>" )
                      .addClass( "arrow" )
                      .addClass( feedback.vertical )
                      .addClass( feedback.horizontal )
                      .appendTo( this );
                  }
                }
              });


              $cookies.webUICookie = $stateParams.username;

                  //queuefactory is a set of service functions, getQueues is one of the queuefactory functions
                  QueueFactory.getQueues($stateParams.username).success(function(data, status, headers, config) {//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
                      //if the getQueues function is successful.. do the following
                      d.resolve(data.queues);
                      // $rootScope.loading = false;
                  }).error(function(data, status, headers, config) {
                      //if the getQueues function fails..do the following
                      //304has been agreed upon as the HTTP return code for when the NERSC credentials are valid
                      //but remoting has not been setup. Upon encountering this error, we automatically set remoting up on the server
                      //and take the user to his queues page. 
                      if(status === 308 || status === 309) {
                          
                          // turn on the loading message
                          $rootScope.dataLoading = true;  
                          // console.log($rootScope.dataLoading)

                          QueueFactory.setupRemote($stateParams.username).then(function(data, status, headers, config) {//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
                              //if the getQueues function is successful.. do the following
                              
                              console.log('remoting set up');
                              $location.path( '/home/' + $stateParams.username );

                          }, function(data, status, headers, config) {//if the getQueues function fails..do the following
                              console.log("error: could not set up remoting.");
                              $rootScope.credentialsAuthorized = false;
                              
                          }).finally(function() {
                            //turn off the loading message
                            $rootScope.dataLoading = false;
                          });


                          //   setTimeout(function() {
                          //   //turn off the loading message
                          //   $rootScope.dataLoading = false;
                          //   console.log("2",$rootScope.dataLoading);
                          // }, 3000;)});
                      }

                      console.log("error: could not retreive queues.");
                      $rootScope.credentialsAuthorized = false;
                      // $rootScope.loading = false;
                      $location.path( '/home' );
                      
                  });

                  return d.promise;

           }]
        }
    })
    .state('queue', {
        url: "/home/:username/queues/:queuename",//username and queuename are $stateParams
        templateUrl: "partials/queuepage.html",
        controller: 'queueCtrl',
        resolve: {//resolve tells angularjs to stall loading the template until 'this' stuff has finished
          
          queue: ['$q', 'QueueFactory', '$stateParams', '$rootScope', '$location', 'localStorageService', function($q, QueueFactory, $stateParams, $rootScope, $location, localStorageService){
            // $rootScope.loading = true;
              var d = $q.defer();
              var queues;
                //queuefactory is a set of service functions, getQueue is one of the queuefactory functions

              QueueFactory.getQueue($stateParams.username, $stateParams.queuename).success(function(data, status, headers, config) {
                //if the getQueues function is successful.. do the following
                  //data.queues = queues;
                  d.resolve(data);
                  // $rootScope.loading = false;
              }).error(function(data, status, headers, config) { //if the getQueues function fails..do the following
                  console.log("error: could not retreive queue.");
                  $rootScope.credentialsAuthorized = false;
                  // $rootScope.loading = false;
                  $location.path( '/home' );
                  /*
                  $rootScope.$on("$locationChangeSuccess", function(event) { 
                      localStorageService.remove("token");
                      console.log("cleared token from local storage");
                  });
                  */
              });
               return d.promise;
           }],

          queueTaskDetails: ['$q', 'QueueFactory', '$stateParams', '$rootScope', '$location', 'localStorageService', function($q, QueueFactory, $stateParams, $rootScope, $location, localStorageService){
            // $rootScope.loading = true;
              var d = $q.defer();
                //queuefactory is a set of service functions, getQueueTaskDetails is one of the queuefactory functions
               QueueFactory.getQueueTaskDetails($stateParams.username, $stateParams.queuename).success(function(data, status, headers, config) {
                //if the getQueues function is successful.. do the following
                  d.resolve(data.tasks);
                  // $rootScope.loading = false;
               }).error(function(data, status, headers, config) { //if the getQueues function fails..do the following
                  console.log("error: could not retreive queue task details.");
                  $rootScope.credentialsAuthorized = false;
                  // $rootScope.loading = false;
                  $location.path( '/home' );
                  /*
                  $rootScope.$on("$locationChangeSuccess", function(event) { 
                      localStorageService.remove("token");
                      console.log("cleared token from local storage");
                  });
                  */
              });
               return d.promise;
           }],

           queues: ['$q', 'QueueFactory', '$stateParams', '$rootScope', '$location', 'localStorageService', '$cookies', function($q, QueueFactory, $stateParams, $rootScope, $location, localStorageService, $cookies){
              // $rootScope.loading = true;
              var d = $q.defer();


              $cookies.webUICookie = $stateParams.username;

                  //queuefactory is a set of service functions, getQueues is one of the queuefactory functions
                  QueueFactory.getQueues($stateParams.username).success(function(data, status, headers, config) {//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
                      //if the getQueues function is successful.. do the following
                      d.resolve(data.queues);
                      // $rootScope.loading = false;
                  }).error(function(data, status, headers, config) {
                      
                      console.log("error: could not retreive queues.");
                      $rootScope.credentialsAuthorized = false;
                      // $rootScope.loading = false;
                      $location.path( '/home' );
                      
                  });

                  return d.promise;

           }]


        }
    })

    .state( 'failed', {
      url: "/home/:username/failed",
      templateUrl: "partials/failedtasks.html",
      controller: 'failedTasksCtrl',
      resolve: { //resolve tells angularjs to stall loading the template until 'this' stuff has finished
          //'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
          queues: ['$q', 'QueueFactory', '$stateParams', '$rootScope', '$location', 'localStorageService', '$cookies', function($q, QueueFactory, $stateParams, $rootScope, $location, localStorageService, $cookies){
              // $rootScope.loading = true;
              var d = $q.defer();


              $cookies.webUICookie = $stateParams.username;
              $stateParams.pageState = 'Failed';
                  //queuefactory is a set of service functions, getQueues is one of the queuefactory functions
                  QueueFactory.getQueues($stateParams.username).success(function(data, status, headers, config) {//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
                      //if the getQueues function is successful.. do the following

                      d.resolve(data.queues);
                      // $rootScope.loading = false;
                  }).error(function(data, status, headers, config) {
                      //if the getQueues function fails..do the following
                      //304has been agreed upon as the HTTP return code for when the NERSC credentials are valid
                      //but remoting has not been setup. Upon encountering this error, we automatically set remoting up on the server
                      //and take the user to his queues page. 
                      if(status === 308 || status === 309) {
                          
                          // turn on the loading message
                          $rootScope.dataLoading = true;  
                          // console.log($rootScope.dataLoading)

                          QueueFactory.setupRemote($stateParams.username).then(function(data, status, headers, config) {//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
                              //if the getQueues function is successful.. do the following
                              
                              console.log('remoting set up');
                              $location.path( '/home/' + $stateParams.username );

                          }, function(data, status, headers, config) {//if the getQueues function fails..do the following
                              console.log("error: could not set up remoting.");
                              $rootScope.credentialsAuthorized = false;
                              
                          }).finally(function() {
                            //turn off the loading message
                            $rootScope.dataLoading = false;
                          });


                          //   setTimeout(function() {
                          //   //turn off the loading message
                          //   $rootScope.dataLoading = false;
                          //   console.log("2",$rootScope.dataLoading);
                          // }, 3000;)});
                      }

                      console.log("error: could not retreive queues.");
                      $rootScope.credentialsAuthorized = false;
                      // $rootScope.loading = false;
                      $location.path( '/home' );
                      
                  });

                  return d.promise;

          }],

        }
    })

    .state( 'succeeded', {
      url: "/home/:username/succeeded",
      templateUrl: "partials/succeeded.html",
      controller: 'succeededTasksCtrl',
      resolve: { //resolve tells angularjs to stall loading the template until 'this' stuff has finished
          //'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
          queues: ['$q', 'QueueFactory', '$stateParams', '$rootScope', '$location', 'localStorageService', '$cookies', function($q, QueueFactory, $stateParams, $rootScope, $location, localStorageService, $cookies){
              // $rootScope.loading = true;
              var d = $q.defer();


              $cookies.webUICookie = $stateParams.username;
              $stateParams.pageState = 'Succeeded';
                  //queuefactory is a set of service functions, getQueues is one of the queuefactory functions
                  QueueFactory.getQueues($stateParams.username).success(function(data, status, headers, config) {//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
                      //if the getQueues function is successful.. do the following

                      d.resolve(data.queues);
                      // $rootScope.loading = false;
                  }).error(function(data, status, headers, config) {
                      //if the getQueues function fails..do the following
                      //304has been agreed upon as the HTTP return code for when the NERSC credentials are valid
                      //but remoting has not been setup. Upon encountering this error, we automatically set remoting up on the server
                      //and take the user to his queues page. 
                      if(status === 308 || status === 309) {
                          
                          // turn on the loading message
                          $rootScope.dataLoading = true;  
                          // console.log($rootScope.dataLoading)

                          QueueFactory.setupRemote($stateParams.username).then(function(data, status, headers, config) {//'inject' all kinds of objects you are going to use, think of this as giving angularJS a heads up.
                              //if the getQueues function is successful.. do the following
                              
                              console.log('remoting set up');
                              $location.path( '/home/' + $stateParams.username );

                          }, function(data, status, headers, config) {//if the getQueues function fails..do the following
                              console.log("error: could not set up remoting.");
                              $rootScope.credentialsAuthorized = false;
                              
                          }).finally(function() {
                            //turn off the loading message
                            $rootScope.dataLoading = false;
                          });


                          //   setTimeout(function() {
                          //   //turn off the loading message
                          //   $rootScope.dataLoading = false;
                          //   console.log("2",$rootScope.dataLoading);
                          // }, 3000;)});
                      }

                      console.log("error: could not retreive queues.");
                      $rootScope.credentialsAuthorized = false;
                      // $rootScope.loading = false;
                      $location.path( '/home' );
                      
                  });

                  return d.promise;

          }],

        }
    })
    
});

qdoApp.config(function(paginationTemplateProvider) {
    paginationTemplateProvider.setPath('../pagination/pagination.html');
});

qdoApp.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
])


// configure the load-bar
qdoApp.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    // turn off load-bar spinner
    cfpLoadingBarProvider.includeSpinner = false;
    // set latency threshold to 300 ms
    cfpLoadingBarProvider.latencyThreshold = 300;
  }])
